//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import android_plugin
import path_provider_foundation
import sqflite

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  AndroidPlugin.register(with: registry.registrar(forPlugin: "AndroidPlugin"))
  PathProviderPlugin.register(with: registry.registrar(forPlugin: "PathProviderPlugin"))
  SqflitePlugin.register(with: registry.registrar(forPlugin: "SqflitePlugin"))
}
