# questions_task_app

A new Flutter project.

## Getting Started
Create Flutter application with two screens to consume Stack exchange APIS
https://api.stackexchange.com/docs
to show questions on the first screen and when user tab on one question 
show the details  of that question on the second screen with all the 
question details that you can show
point to consider:
- Clean and elegant UI
- Clean code architecture
- try to implement pagination and lazy loader
- User can access the questions with offline mode [Please use sqlite] (show the already loaded questions)
(Android Related Task)
- Create a plugin using flutter to work with android platform (Kotlin or Java).
Try to consume one of stack exchange APIS and show a toast with success message after successfully retrieving data .
Upload the plugin into github and use it in Flutter task app , activate it with floating action button .

after you complete the application push your code into git hub and share 
the repository URL (make sure the repository is public ) and application 
APK by email.

## :)
