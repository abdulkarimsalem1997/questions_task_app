import 'package:flutter_cache_manager/flutter_cache_manager.dart';
  import 'dart:io';

  
// to save image with out internet connection
class ImageCacheManager  {
  static Future<File> getImagePath(String imageUrl) async {
    final file = await DefaultCacheManager().getSingleFile(imageUrl);
    return file;
  
}
}