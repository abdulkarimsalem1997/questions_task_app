import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:questions_task_app/core/network/network_info.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:questions_task_app/featues/questions/domain/usecases/get_all_questions.dart';
import 'package:questions_task_app/featues/questions/domain/repositories/questions_repository.dart';
import 'package:questions_task_app/featues/questions/data/repositories/question_repository_impl.dart';
import 'package:questions_task_app/featues/questions/data/datasources/question_local_data_source.dart';
import 'package:questions_task_app/featues/questions/data/datasources/question_remote_data_source.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questions/bloc/questions_bloc.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questionDetails/bloc/question_details_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
// Features - questions

// Bolc
  sl.registerFactory(() => QuestionsBloc(getAllQuestions: sl()));
  sl.registerFactory(() => QuestionDetailsBloc());

// Usecases
  sl.registerLazySingleton(() => GetAllQuestionsUsecase(repository: sl()));
// Repository
  sl.registerLazySingleton<QuestionsRepository>(() => QuestionsRepositoryImpl(
      remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()));
// Datasources
  sl.registerLazySingleton<QuestionRemoteDataSource>(
      () => QuestionRemoteDataSourceImpl(client: sl()));
  sl.registerLazySingleton<QuestionLocalDataSource>(
      () => QuestionLocalDataSourceImpl());
// Core
sl.registerLazySingleton<NetworkInfo>(()=>NetworkInfoImpl(sl()));
// External
 sl.registerLazySingleton(() => http.Client());
 sl.registerLazySingleton(() => InternetConnectionChecker());
}
