import 'package:questions_task_app/featues/questions/domain/entities/question.dart';

class QuestionModel extends Question {
  const QuestionModel({
    required super.questionId,
    required super.title,
    required super.author,
    required super.creationDate,
    required super.link,
    required super.profileImage,
  });

  factory QuestionModel.fromJson(Map<String, dynamic> json, {isFromSql =false}) {

    return QuestionModel(
      questionId: json['question_id'],
      title: json['title'],
      author: isFromSql ?json['author']:json['owner']['display_name'],
      profileImage: isFromSql ?json['profile_image']: json['owner']['profile_image'],
      link: json['link'],
      creationDate:isFromSql ? DateTime.fromMillisecondsSinceEpoch(json['creationDate'] * 1000):
          DateTime.fromMillisecondsSinceEpoch(json['creation_date'] * 1000),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'question_id': questionId,
      'title': title,
      'author': author,
      'profile_image': profileImage,
      'link': link,
      'creationDate': creationDate.millisecondsSinceEpoch,
    };
  }
}
