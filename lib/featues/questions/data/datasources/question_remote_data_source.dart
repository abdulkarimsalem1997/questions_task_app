import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:questions_task_app/core/error/exceptions.dart';
import 'package:questions_task_app/featues/questions/data/models/question_model.dart';
// ignore_for_file: constant_identifier_names

abstract class QuestionRemoteDataSource {
  Future<List<QuestionModel>> getAllQuestions({int? firstPage});
}

const BASE_URL = "https://api.stackexchange.com/2.3";

class QuestionRemoteDataSourceImpl extends QuestionRemoteDataSource {
  final http.Client client;
  int page = 1;
  QuestionRemoteDataSourceImpl({required this.client});

  @override
  Future<List<QuestionModel>> getAllQuestions({int? firstPage}) async {
    page++;
    if (firstPage != null) page = firstPage;

    final response = await client.get(
      Uri.parse(
          "$BASE_URL/questions?page=$page&order=desc&sort=activity&site=stackoverflow"),
      // headers: {"Content-Type": "application/json"},
    );

    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      final List<dynamic> questionsJson = body['items'] as List;
      final List<QuestionModel> postModels = questionsJson
          .map<QuestionModel>(
              (jsonQuestionModel) => QuestionModel.fromJson(jsonQuestionModel))
          .toList();

      return postModels;
    } else {
      throw ServerException();
    }
  }
}
