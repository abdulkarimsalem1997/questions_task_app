import 'package:path/path.dart';
import 'package:dartz/dartz.dart';
import 'package:sqflite/sqflite.dart';
import 'package:questions_task_app/core/error/exceptions.dart';
import 'package:questions_task_app/featues/questions/data/models/question_model.dart';
// ignore_for_file: constant_identifier_names

abstract class QuestionLocalDataSource {
  Future<List<QuestionModel>> getCachedQuestions();
  Future<Unit> cacheQuestions(List<QuestionModel> questionModels);
}

const Database_Name = "Questions.db";

const Table_Name = "questions";

class QuestionLocalDataSourceImpl extends QuestionLocalDataSource {
  static final QuestionLocalDataSourceImpl _instance =
      QuestionLocalDataSourceImpl._internal();
  factory QuestionLocalDataSourceImpl() => _instance;
  QuestionLocalDataSourceImpl._internal();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    String path = join(await getDatabasesPath(), 'questions.db');
    return await openDatabase(
      path,
      version: 3,
      onCreate: _onCreate,
      onUpgrade: (db, oldVersion, newVersion) async {
        if (oldVersion < 3) {
          await db.execute("ALTER TABLE questions ADD COLUMN link TEXT;");
        }
      },
    );
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE questions(
        question_id INTEGER PRIMARY KEY,
        title TEXT,
        author TEXT,
        profile_image TEXT,
        link TEXT,
        creationDate INTEGER
      )
    ''');
  }

  @override
  Future<Unit> cacheQuestions(List<QuestionModel> questionModels) async {
    final db = await database;
    for (var question in questionModels) {
      await db
          .insert(
            Table_Name,
            question.toJson(),
            conflictAlgorithm: ConflictAlgorithm.replace,
          );
          
    }

    return Future.value(unit);
  }

  @override
  Future<List<QuestionModel>> getCachedQuestions() async {
    final db = await database;
    try {
      final List<Map<String, dynamic>> maps = await db.query(Table_Name);
      if (maps.isEmpty) {
        throw EmptyCacheException();
      }
      return Future.value(List.generate(maps.length, (i) {
        return QuestionModel.fromJson(maps[i],isFromSql: true);
      }));
    } catch (e) {
      throw EmptyCacheException();
    }
  }
 }
