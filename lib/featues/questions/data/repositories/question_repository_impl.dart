import 'package:dartz/dartz.dart';
import 'package:questions_task_app/core/error/failures.dart';
import 'package:questions_task_app/core/error/exceptions.dart';
import 'package:questions_task_app/core/network/network_info.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';
import 'package:questions_task_app/featues/questions/domain/repositories/questions_repository.dart';
import 'package:questions_task_app/featues/questions/data/datasources/question_local_data_source.dart';
import 'package:questions_task_app/featues/questions/data/datasources/question_remote_data_source.dart';

class QuestionsRepositoryImpl extends QuestionsRepository {
  final QuestionRemoteDataSource remoteDataSource;
  final QuestionLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  QuestionsRepositoryImpl(
      {required this.remoteDataSource,
      required this.localDataSource,
      required this.networkInfo});

  @override
  Future<Either<Failure, List<Question>>> getAllQuestions({int? firstPage}) async {
    if (await networkInfo.isConnected) {
      try {
        final remotePosts = await remoteDataSource.getAllQuestions(firstPage: firstPage);
        localDataSource.cacheQuestions(remotePosts);
        return Right(remotePosts);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        print("NoInternet");
        final localPosts = await localDataSource.getCachedQuestions();
        return Right(localPosts);
      } on EmptyCacheException {
        print("EmptyCacheException");
        return Left(EmptyLocalStorageFailure());
      }
    }
  }
}
