import 'package:dartz/dartz.dart';
import '../../../../core/error/failures.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';
import 'package:questions_task_app/featues/questions/domain/repositories/questions_repository.dart';



class GetAllQuestionsUsecase {
  final QuestionsRepository repository;

  GetAllQuestionsUsecase( {required this.repository });

  Future<Either<Failure, List<Question>>> call({int? firstPage}) async {
    return await repository.getAllQuestions(firstPage: firstPage );
  }
}
