import 'package:equatable/equatable.dart';

class Question extends Equatable {
  final int questionId;
  final String title;
  final String author;
  final String? profileImage;
  final String? link;
  final DateTime creationDate;

  const Question( {required this.profileImage,required this.questionId, required this.title,  required this.author,required this.link, required this.creationDate});

  @override
  List<Object?> get props => [questionId, title, author, profileImage ,link,creationDate ];
}
