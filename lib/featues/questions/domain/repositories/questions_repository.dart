import 'package:dartz/dartz.dart';
import 'package:questions_task_app/core/error/failures.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';

abstract class QuestionsRepository {
  Future<Either<Failure, List<Question>>> getAllQuestions({int? firstPage});
 
}
