import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questionDetails/bloc/question_details_bloc.dart';

class QuestionDetailPage extends StatefulWidget {
  final Question question;

  const QuestionDetailPage({super.key, required this.question});

  @override
  State<QuestionDetailPage> createState() => _QuestionDetailPageState();
}

class _QuestionDetailPageState extends State<QuestionDetailPage> {
  late final WebViewController _controller;
  @override
  void initState() {
    // inialize webView
    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.disabled)
      ..loadRequest(Uri.parse(widget.question.link!))
      ..setNavigationDelegate(
        NavigationDelegate(
          onPageStarted: (String url) {
            context.read<QuestionDetailsBloc>().add(PageStartedEvent(url));
          },
          onPageFinished: (String url) {
            context.read<QuestionDetailsBloc>().add(PageFinishedEvent(url));
          },
        ),
      );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.question.title,
          ),
        ),
        body: BlocBuilder<QuestionDetailsBloc, QuestionDetailsState>(
          builder: (context, state) {
            if (state is QuestionDetailsLoadingState) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return WebViewWidget(
                controller: _controller,
              );
            }
          },
        ),
      ),
    );
  }
}
