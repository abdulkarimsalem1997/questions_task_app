import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:android_plugin/android_plugin.dart';
import 'package:questions_task_app/core/widgets/loading_widget.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questions/bloc/questions_bloc.dart';
import 'package:questions_task_app/featues/questions/presentation/widgets/questions_page/question_list_widget.dart';
import 'package:questions_task_app/featues/questions/presentation/widgets/questions_page/message_display_widget.dart';


class QuestionsPage extends StatelessWidget {
  const QuestionsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppbar(),
      body: _buildBody(),
      floatingActionButton: _buildFloatingBtn(context),
    );
  }

  AppBar _buildAppbar() => AppBar(title: const Text('Questions'));

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: BlocConsumer<QuestionsBloc, QuestionsState>(
        listener: (context, state) {
          if (state is LoadedQuestionsState) {
          }
        },
        builder: (context, state) {
          return BlocBuilder<QuestionsBloc, QuestionsState>(
            builder: (context, state) {
              if (state is LoadingQuestionsState) {
                return const LoadingWidget();
              } else if (state is LoadedQuestionsState) {
                return RefreshIndicator(
                    onRefresh: () => _onRefresh(context),
                    child: QuestionsListWidget(questions: state.questions));
              } else if (state is ErrorQuestionsState) {
                return MessageDisplayWidget(message: state.message);
              } else if (state is PagenationQuestionsState) {
                return Stack(
                  children: [
                    QuestionsListWidget(questions: state.questions),
                    const Align(
                      alignment: Alignment.bottomCenter,
                      child: CircularProgressIndicator(),
                    ),
                  ],
                );
              }
              return const LoadingWidget();
            },
          );
        },
      ),
    );
  }

  Widget _buildFloatingBtn(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
          AndroidPlugin.fetchQuestions();
          context.read<QuestionsBloc>().add(const RefreshQuestionsEvent());
           
      },
      child: const Icon(Icons.refresh),
    );
  }

  Future<void> _onRefresh(BuildContext context) async {
    BlocProvider.of<QuestionsBloc>(context).add(const RefreshQuestionsEvent());
  }

}
