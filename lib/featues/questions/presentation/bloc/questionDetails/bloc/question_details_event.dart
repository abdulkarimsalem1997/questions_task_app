part of 'question_details_bloc.dart';

abstract class QuestionDetailsEvent extends Equatable {
  const QuestionDetailsEvent();

  @override
  List<Object> get props => [];
}

class PageStartedEvent extends QuestionDetailsEvent {
  final String url;

  const PageStartedEvent(this.url);
}

class PageFinishedEvent extends QuestionDetailsEvent {
  final String url;

  const PageFinishedEvent(this.url);
}
