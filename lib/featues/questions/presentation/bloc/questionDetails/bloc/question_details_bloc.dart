import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
part 'question_details_event.dart';
part 'question_details_state.dart';

class QuestionDetailsBloc
    extends Bloc<QuestionDetailsEvent, QuestionDetailsState> {
  QuestionDetailsBloc() : super(QuestionDetailsInitialState()) {
    on<QuestionDetailsEvent>((event, emit) {
      if (event is PageStartedEvent) {
        emit(QuestionDetailsLoadingState());
      } else if (event is PageFinishedEvent) {
        emit(QuestionDetailsLoadedState());
      }
    });
  }
}
