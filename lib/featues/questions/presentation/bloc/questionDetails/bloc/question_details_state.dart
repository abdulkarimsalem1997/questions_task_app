part of 'question_details_bloc.dart';

abstract class QuestionDetailsState extends Equatable {
  const QuestionDetailsState();
  
  @override
  List<Object> get props => [];
}

class QuestionDetailsInitialState extends QuestionDetailsState {}

class QuestionDetailsLoadingState extends QuestionDetailsState {}

class QuestionDetailsLoadedState extends QuestionDetailsState {}