import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:questions_task_app/core/error/failures.dart';
import 'package:questions_task_app/core/strings/failures.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';
import 'package:questions_task_app/featues/questions/domain/usecases/get_all_questions.dart';
// ignore_for_file: type_literal_in_constant_pattern

part 'questions_event.dart';
part 'questions_state.dart';



class QuestionsBloc extends Bloc<QuestionsEvent, QuestionsState> {
  final GetAllQuestionsUsecase getAllQuestions;
  final listOfQuestion = <Question>[];
  QuestionsBloc({required this.getAllQuestions}) : super(QuestionsInitial()) {
    on<QuestionsEvent>((event, emit) async {
      if (event is GetAllQuestionsEvent) {
        emit(LoadingQuestionsState());

        final failureOrQuestions = await getAllQuestions(firstPage: 1);
        emit(  _mapFailureOrQuestionsToState(failureOrQuestions));
      }
      else if (event is PagenationQuestionsEvent) {
        emit(PagenationQuestionsState(questions: listOfQuestion));
        final failureOrQuestions = await getAllQuestions();
        emit(_mapFailureOrQuestionsToState(failureOrQuestions));
      } 
      
      else if (event is RefreshQuestionsEvent) {
        emit(LoadingQuestionsState());

        final failureOrQuestions = await getAllQuestions(firstPage: 1);
        emit( _mapFailureOrQuestionsToState(failureOrQuestions));
      }
    }, transformer: droppable());
  }

  QuestionsState _mapFailureOrQuestionsToState(
      Either<Failure, List<Question>> either) {
    return either.fold(
      (failure) => ErrorQuestionsState(message: _mapFailureToMessage(failure)),
      (questions) {
        listOfQuestion.addAll(questions);
        return LoadedQuestionsState(
        questions: listOfQuestion,
      );
      } 
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case EmptyLocalStorageFailure:
        return EMPTY_CACHE_FAILURE_MESSAGE;
      case OfflineFailure:
        return OFFLINE_FAILURE_MESSAGE;
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}