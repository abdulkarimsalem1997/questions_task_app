part of 'questions_bloc.dart';

abstract class QuestionsEvent extends Equatable {
  const QuestionsEvent();

  @override
  List<Object> get props => [];
}

class GetAllQuestionsEvent extends QuestionsEvent {
  

  const GetAllQuestionsEvent();
}

class RefreshQuestionsEvent extends QuestionsEvent {
  

  const RefreshQuestionsEvent();
}

class PagenationQuestionsEvent extends QuestionsEvent {
  

  const PagenationQuestionsEvent();
}
