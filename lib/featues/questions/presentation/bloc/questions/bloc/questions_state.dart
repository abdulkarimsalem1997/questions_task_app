part of 'questions_bloc.dart';

abstract class QuestionsState extends Equatable {
  const QuestionsState();

  @override
  List<Object> get props => [];
}

class QuestionsInitial extends QuestionsState {}

class LoadingQuestionsState extends QuestionsState {}

class LoadedQuestionsState extends QuestionsState {
  final List<Question> questions;

  const LoadedQuestionsState({required this.questions});

  @override
  List<Object> get props => [questions];
}
class PagenationQuestionsState extends QuestionsState {
  final List<Question> questions;

  const PagenationQuestionsState({required this.questions});

  @override
  List<Object> get props => [questions];
}

class ErrorQuestionsState extends QuestionsState {
  final String message;

  const ErrorQuestionsState({required this.message});

  @override
  List<Object> get props => [message];
}
