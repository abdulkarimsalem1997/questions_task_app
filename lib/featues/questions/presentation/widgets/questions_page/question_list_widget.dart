import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:questions_task_app/core/util/image_cache_manager.dart';
import 'package:questions_task_app/featues/questions/domain/entities/question.dart';
import 'package:questions_task_app/featues/questions/presentation/pages/question_detail_page.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questions/bloc/questions_bloc.dart';

class QuestionsListWidget extends StatefulWidget {
  final List<Question> questions;

  const QuestionsListWidget({
    super.key,
    required this.questions,
  });

  @override
  State<QuestionsListWidget> createState() => _QuestionsListWidgetState();
}

class _QuestionsListWidgetState extends State<QuestionsListWidget> {

  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {

    _scrollController.addListener(() {
      // check if user scrolled to the end of the list
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _loadMoreQuestions();
      }
    });
    super.initState();
  }

  void _loadMoreQuestions() {
  
    context.read<QuestionsBloc>().add(const PagenationQuestionsEvent());
    
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      ListView.separated(
        shrinkWrap: true,
        controller: _scrollController,
        itemCount: widget.questions.length,
        itemBuilder: (context, index) {
        
            return ListTile(
              leading: widget.questions[index].profileImage != null
                  ? Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: FutureBuilder(
                          future: ImageCacheManager.getImagePath(
                            widget.questions[index].profileImage!,
                          ),
                          builder: (context, snapshot) {
                            if (snapshot.data != null) {
                              return Image.file(snapshot.data!);
                            } else {
                              return Image.asset(
                                  'assets/images/fallback_image.png');
                            }
                          }),
                    )
                  : null,
              title: Text(
                widget.questions[index].author,
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                widget.questions[index].title,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
              contentPadding: const EdgeInsets.symmetric(horizontal: 10),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) =>
                        QuestionDetailPage(question: widget.questions[index]),
                  ),
                );
              },
            );
          
          
        },
        separatorBuilder: (context, index) => const Divider(thickness: 1),
      ),
     
    ]);
  }


}
