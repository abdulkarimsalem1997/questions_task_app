import 'package:flutter/material.dart';
import 'injection_container.dart' as di;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:questions_task_app/core/app_theme.dart';
import 'package:questions_task_app/core/util/my_bloc_observer.dart';
import 'package:questions_task_app/featues/questions/presentation/pages/questions_page.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questions/bloc/questions_bloc.dart';
import 'package:questions_task_app/featues/questions/presentation/bloc/questionDetails/bloc/question_details_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) =>
              di.sl<QuestionsBloc>()..add(const GetAllQuestionsEvent()),
        ),
        BlocProvider(
          create: (context) => di.sl<QuestionDetailsBloc>(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: appTheme,
        title: 'Stack Exchange App',
        home: const QuestionsPage(),
      ),
    );
  }
}
